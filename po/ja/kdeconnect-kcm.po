# <tomhioo@outlook.jp>, 2019.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2019.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-07 00:46+0000\n"
"PO-Revision-Date: 2022-08-24 22:31+0900\n"
"Last-Translator: Ryuichi Yamada <ryuichi_ya220@outlook.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 22.04.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Ryuichi Yamada"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ryuichi_ya220@outlook.jp"

#: kcm.cpp:38
#, kde-format
msgid "KDE Connect Settings"
msgstr "KDE Connect の設定"

#: kcm.cpp:40
#, kde-format
msgid "KDE Connect Settings module"
msgstr "KDE Connect の設定モジュール"

#: kcm.cpp:42
#, kde-format
msgid "(C) 2015 Albert Vaca Cintora"
msgstr "(C) 2015 Albert Vaca Cintora"

#: kcm.cpp:45
#, kde-format
msgid "Albert Vaca Cintora"
msgstr "Albert Vaca Cintora"

#: kcm.cpp:224
#, kde-format
msgid "Key: %1"
msgstr "キー: %1"

#: kcm.cpp:255
#, kde-format
msgid "Available plugins"
msgstr "利用可能なプラグイン"

#: kcm.cpp:308
#, kde-format
msgid "Error trying to pair: %1"
msgstr "ペアリング試行エラー: %1"

#: kcm.cpp:329
#, kde-format
msgid "(paired)"
msgstr "(ペアリング済み)"

#: kcm.cpp:332
#, kde-format
msgid "(not paired)"
msgstr "(ペアリングしていません)"

#: kcm.cpp:335
#, kde-format
msgid "(incoming pair request)"
msgstr "(ペアリング要求を受信しました)"

#: kcm.cpp:338
#, kde-format
msgid "(pairing requested)"
msgstr "(ペアリング要求済み)"

#. i18n: ectx: property (text), widget (QLabel, rename_label)
#: kcm.ui:59
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#. i18n: ectx: property (text), widget (QToolButton, renameShow_button)
#: kcm.ui:82
#, kde-format
msgid "Edit"
msgstr "編集"

#. i18n: ectx: property (text), widget (QToolButton, renameDone_button)
#: kcm.ui:104
#, kde-format
msgid "Save"
msgstr "保存"

#. i18n: ectx: property (text), widget (QPushButton, refresh_button)
#: kcm.ui:120
#, kde-format
msgid "Refresh"
msgstr "再読み込み"

#. i18n: ectx: property (text), widget (QLabel, name_label)
#: kcm.ui:193
#, kde-format
msgid "Device"
msgstr "デバイス"

#. i18n: ectx: property (text), widget (QLabel, status_label)
#: kcm.ui:209
#, kde-format
msgid "(status)"
msgstr "(状態)"

#. i18n: ectx: property (text), widget (KSqueezedTextLabel, verificationKey)
#: kcm.ui:232
#, kde-format
msgid "🔑 abababab"
msgstr "🔑 abababab"

#. i18n: ectx: property (text), widget (QPushButton, accept_button)
#: kcm.ui:276
#, kde-format
msgid "Accept"
msgstr "承認"

#. i18n: ectx: property (text), widget (QPushButton, reject_button)
#: kcm.ui:283
#, kde-format
msgid "Reject"
msgstr "拒否"

#. i18n: ectx: property (text), widget (QPushButton, pair_button)
#: kcm.ui:296
#, kde-format
msgid "Request pair"
msgstr "ペアリングを要求"

#. i18n: ectx: property (text), widget (QPushButton, unpair_button)
#: kcm.ui:309
#, kde-format
msgid "Unpair"
msgstr "ペアリング解除"

#. i18n: ectx: property (text), widget (QPushButton, ping_button)
#: kcm.ui:322
#, kde-format
msgid "Send ping"
msgstr "Ping を送信"

#. i18n: ectx: property (text), widget (QLabel, noDeviceLinks)
#: kcm.ui:360
#, kde-format
msgid ""
"<html><head/><body><p>No device selected.<br><br>If you own an Android "
"device, make sure to install the <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE Connect Android app</span></a> (also "
"available <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
"kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">from F-Droid</span></a>) and it should appear in the list.<br><br>If you "
"are having problems, visit the <a href=\"https://userbase.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">KDE Connect "
"Community wiki</span></a> for help.</p></body></html>"
msgstr ""
"<html><head/><body><p>デバイスが選択されていません。<br><br>もし Android デバ"
"イスを所有している場合、<a href=\"https://play.google.com/store/apps/details?"
"id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: underline; color:"
"#4c6b8a;\">KDE Connect の Android アプリ</span></a> (<a href=\"https://f-"
"droid.org/repository/browse/?fdid=org.kde.kdeconnect_tp\"><span style=\" "
"text-decoration: underline; color:#4c6b8a;\">F-Droid</span></a> からも入手可"
"能) をインストールしてください。インストール後、こちらのリストに表示されま"
"す。<br><br>もし問題が発生し、サポートが必要な場合は、<a href=\"https://"
"userbase.kde.org/KDEConnect\"><span style=\" text-decoration: underline; "
"color:#4c6b8a;\">KDE Connect Community wiki</span></a>をご覧ください</p></"
"body></html>"
