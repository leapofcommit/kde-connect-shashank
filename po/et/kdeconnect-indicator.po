# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Marek Laane <qiilaq69@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2019-12-24 15:13+0200\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Marek Laane"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "qiilaq69@gmail.com"

#: deviceindicator.cpp:53
#, kde-format
msgid "Browse device"
msgstr "Sirvi seadet"

#: deviceindicator.cpp:67
#, kde-format
msgid "Ring device"
msgstr "Helista seadmele"

#: deviceindicator.cpp:81
#, kde-format
msgid "Get a photo"
msgstr ""

#: deviceindicator.cpp:101
#, fuzzy, kde-format
#| msgid "Send file"
msgid "Send a file/URL"
msgstr "Saada fail"

#: deviceindicator.cpp:111
#, kde-format
msgid "SMS Messages..."
msgstr "SMS-sõnumid ..."

#: deviceindicator.cpp:124
#, kde-format
msgid "Run command"
msgstr "Käivita käsk"

#: deviceindicator.cpp:126
#, kde-format
msgid "Add commands"
msgstr "Lisa käsud"

#: indicatorhelper_mac.cpp:34
#, kde-format
msgid "Launching"
msgstr "Käivitamine"

#: indicatorhelper_mac.cpp:84
#, kde-format
msgid "Launching daemon"
msgstr "Deemoni käivitamine"

#: indicatorhelper_mac.cpp:93
#, kde-format
msgid "Waiting D-Bus"
msgstr "Oodatakse D-Busi järel"

#: indicatorhelper_mac.cpp:110 indicatorhelper_mac.cpp:120
#: indicatorhelper_mac.cpp:135
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: indicatorhelper_mac.cpp:111 indicatorhelper_mac.cpp:121
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"Ühendus D-Busiga nurjus\n"
" KDE Connect lõpetab töö"

#: indicatorhelper_mac.cpp:135
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "kdeconnectd'd ei leitud"

#: indicatorhelper_mac.cpp:140
#, kde-format
msgid "Loading modules"
msgstr "Moodulite laadimine"

#: main.cpp:41
#, kde-format
msgid "KDE Connect Indicator"
msgstr "KDE Connecti indikaator"

#: main.cpp:43
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "KDE Connecti indikaator"

#: main.cpp:45
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "(C) 2016: Aleix Pol Gonzalez"

#: main.cpp:80
#, kde-format
msgid "Configure..."
msgstr "Seadista ..."

#: main.cpp:102
#, kde-format
msgid "Pairing requests"
msgstr "Paaristumissoovid"

#: main.cpp:107
#, kde-format
msgid "Pair"
msgstr "Paaristu"

#: main.cpp:108
#, kde-format
msgid "Reject"
msgstr "Lükka tagasi"

#: main.cpp:114 main.cpp:124
#, kde-format
msgid "Quit"
msgstr "Välju"

#: main.cpp:143 main.cpp:167
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "Ühendatud on %1 seade"
msgstr[1] "Ühendatud on %1 seadet"

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr "Aku puudub"

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "Aku: %1 (laeb)"

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr "Aku: %1%"

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr ""

#: systray_actions/connectivity_action.cpp:33
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr ""

#~ msgid "Select file to send to '%1'"
#~ msgstr "Seadmele \"%1\" saadetava faili valimine"
