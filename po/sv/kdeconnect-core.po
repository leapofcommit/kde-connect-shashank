# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2014, 2015, 2016, 2018, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-02-01 08:35+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: backends/bluetooth/bluetoothpairinghandler.cpp:56
#: backends/lan/lanpairinghandler.cpp:52
#, kde-format
msgid "Canceled by other peer"
msgstr "Avbruten av motparten"

#: backends/bluetooth/bluetoothpairinghandler.cpp:65
#: backends/lan/lanpairinghandler.cpp:61
#, kde-format
msgid "%1: Already paired"
msgstr "%1: Redan ihopparade"

#: backends/bluetooth/bluetoothpairinghandler.cpp:68
#, kde-format
msgid "%1: Pairing already requested for this device"
msgstr "%1: Ihopparning redan begärd för apparaten"

#: backends/bluetooth/bluetoothpairinghandler.cpp:124
#: backends/lan/lanpairinghandler.cpp:106
#, kde-format
msgid "Timed out"
msgstr "Tidsgräns överskriden"

#: backends/lan/compositeuploadjob.cpp:82
#, kde-format
msgid "Couldn't find an available port"
msgstr "Kunde inte hitta en tillgänglig port"

#: backends/lan/compositeuploadjob.cpp:121
#, kde-format
msgid "Failed to send packet to %1"
msgstr "Misslyckades skicka paket till %1"

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "Sending to %1"
msgstr "Skickar till %1"

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "File"
msgstr "Fil"

#: backends/lan/landevicelink.cpp:146 backends/lan/landevicelink.cpp:160
#, kde-format
msgid ""
"This device cannot be paired because it is running an old version of KDE "
"Connect."
msgstr ""
"Apparaten kan inte paras ihop eftersom den kör en gammal version av KDE-"
"anslut."

#: compositefiletransferjob.cpp:46
#, kde-format
msgctxt "@title job"
msgid "Receiving file"
msgid_plural "Receiving files"
msgstr[0] "Tar emot fil"
msgstr[1] "Tar emot filer"

#: compositefiletransferjob.cpp:47
#, kde-format
msgctxt "The source of a file operation"
msgid "Source"
msgstr "Källa"

#: compositefiletransferjob.cpp:48
#, kde-format
msgctxt "The destination of a file operation"
msgid "Destination"
msgstr "Mål"

#: device.cpp:215
#, kde-format
msgid "Already paired"
msgstr "Redan ihopparade"

#: device.cpp:220
#, kde-format
msgid "Device not reachable"
msgstr "Apparaten kan inte nås"

#: device.cpp:561
#, kde-format
msgid "SHA256 fingerprint of your device certificate is: %1\n"
msgstr "SHA256-fingeravtryck för din apparats certifikat är: %1\n"

#: device.cpp:569
#, kde-format
msgid "SHA256 fingerprint of remote device certificate is: %1\n"
msgstr "SHA256-fingeravtryck för den andra apparatens certifikat är: %1\n"

#: filetransferjob.cpp:51
#, kde-format
msgid "Filename already present"
msgstr "Filnamnet finns redan"

#: filetransferjob.cpp:100
#, kde-format
msgid "Received incomplete file: %1"
msgstr "Mottog ofullständig fil: %1"

#: filetransferjob.cpp:118
#, kde-format
msgid "Received incomplete file from: %1"
msgstr "Mottog ofullständig fil från: %1"

#: kdeconnectconfig.cpp:76
#, kde-format
msgid "KDE Connect failed to start"
msgstr "KDE-anslut misslyckades starta"

#: kdeconnectconfig.cpp:77
#, kde-format
msgid ""
"Could not find support for RSA in your QCA installation. If your "
"distribution provides separate packets for QCA-ossl and QCA-gnupg, make sure "
"you have them installed and try again."
msgstr ""
"Kunde inte hitta stöd för RSA i installationen av QCA. Om distributionen "
"tillhandahåller separata paket för QCA-ossl och QCA-gnupg, försäkra dig om "
"att de är installerade och försök igen."

#: kdeconnectconfig.cpp:313
#, kde-format
msgid "Could not store private key file: %1"
msgstr "Kunde inte lagra privat nyckelfil: %1"

#: kdeconnectconfig.cpp:358
#, kde-format
msgid "Could not store certificate file: %1"
msgstr "Kunde inte lagra certifikatfil: %1"

#~ msgid "Sent 1 file"
#~ msgid_plural "Sent %1 files"
#~ msgstr[0] "Skickade 1 fil"
#~ msgstr[1] "Skickade %1 filer"

#~ msgid "Progress"
#~ msgstr "Förlopp"

#~ msgid "Sending file %1 of %2"
#~ msgstr "Skickar fil %1 av %2"

#~ msgid "Receiving file %1 of %2"
#~ msgstr "Tar emot fil %1 av %2"

#~ msgid "Receiving file over KDE Connect"
#~ msgstr "Tar emot fil via KDE-anslut"

#~ msgctxt "File transfer origin"
#~ msgid "From"
#~ msgstr "Från"

#~ msgctxt "File transfer destination"
#~ msgid "To"
#~ msgstr "Till"

#~ msgid "Finished sending to %1"
#~ msgstr "Sändning till %1 klar"

#~ msgid "Error contacting device"
#~ msgstr "Fel vid försök att kontakta apparaten"

#~ msgid "Received incorrect key"
#~ msgstr "Mottog felaktig nyckel"

#~ msgid "Canceled by the user"
#~ msgstr "Avbruten av användaren"

#~ msgid "Pairing request from %1"
#~ msgstr "Begäran om ihopparning från %1"

#~ msgid "Accept"
#~ msgstr "Acceptera"

#~ msgid "Reject"
#~ msgstr "Avslå"

#~ msgid "Incoming file exists"
#~ msgstr "Inkommande fil finns"

#~ msgctxt "Device name that will appear on the jobs"
#~ msgid "KDE-Connect"
#~ msgstr "KDE-anslut"
